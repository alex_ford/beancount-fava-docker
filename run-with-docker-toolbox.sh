#!/bin/bash

# ATTENTION: this currently only works when used with the Docker Quick
# Start Terminal. It does not work in PowerShell or Bash for Windows (yet).
# [] Figure out how this script can work in PowerShell (or the .ps1 script)
# [] Figure out how this script can work in Bash for Windows (e.g. bash .\runDockerToolbox.sh)

# This script was intended to be run using the Docker Quick Start Terminal
# which provides an emulated bash environment on Windows.
#
# @author Alex Ford (arf4188@gmail.com)

# TODO provide beginnings of a 'cli' here, with a start (default) and stop command

# Start the container
# Tip: remove the -d switch to run in attached mode and see console output from the container
export cid=`docker ps --all --quiet --filter name=fava`
if [[ -z ${cid} ]]; then
    docker run --rm -p 5000:5000 \
        -v `pwd`:/data \
        -e BEANCOUNT_INPUT_FILE=/data/ledger.beancount \
        --name fava \
        -d \
        yegle/fava
    
    # Wait for container to be ready
    echo Please wait while the container starts...
    # TODO make this actually poll the container somehow
    sleep 5
else
    echo Found Docker container ${cid} for existing Beancount/Fava application.
fi

# Print the URL that will access Fava; and open a browser automatically
# TODO determine the URL dynamically so this can be more portable across Docker/system configurations
echo "http://192.168.99.100:5000"
# TODO way to reference default browser?
# TODO make use of the dynamically detected URL
# TODO can we provide a "Chrome App" option?
#   The --app switch isn't working for some reason...
/c/Program\ Files\ \(x86\)/Google/Chrome/Application/chrome.exe 192.168.99.100:5000 --profile-directory=Default --app &

echo "Consider shutting down the container when finished, as this should save battery life considerably!"
echo "./stopDocker.sh"
echo "./git-sync.sh"