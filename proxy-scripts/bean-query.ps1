# Proxy script for running the command of the
# same name as this script, inside a docker container
#
# Consider setting a PowerShell alias to make this even
# more convenient.
#
# @author Alex Ford (arf4188@gmail.com)

Write-Debug "PSScriptRoot is: ${PSScriptRoot}"
. ${PSScriptRoot}\config.ps1

if (`docker-machine status` == "Stopped") {
    Write-Output "Docker Machine not started... attempting to start it now."
    docker-machine start
}

Write-Output "Using query: ${args}"
docker exec --workdir /data ${CONTAINER_NAME} bean-query -q ledger.beancount $args
