# Simple proxy script to run the bean-report tool from inside the container.
# @author Alex Richard Ford (arf4188@gmail.com)

. .\config.ps1
docker exec -it --workdir /data ${CONTAINER_NAME} bean-report $args
