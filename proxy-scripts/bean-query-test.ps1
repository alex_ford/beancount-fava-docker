# The following demonstrates how to use the bean-query.ps1 proxy script to execute
# a BQL query inside a dockerized beancount/fava environment.
#
# @author Alex Richard Ford (arf4188@gmail.com)
# @website http://www.alexrichardford.com

# TIP: Powershell allows you to continue a statement on the next line using the ` character.
# TIP: The entire BQL query must be appropriately wrapped in "" characters.
# This query pulls up a "register/ledger" view of the named account, much like what you would
# see if you opened up Fava and went to the Balances view. It includes each transaction in the
# account, along with the running balance.
. .\bean-query.ps1 "SELECT date, flag, payee, narration, position, balance" `
                   "WHERE account ~ 'Assets:Banking:Ally:Checking' AND" `
                      "date >= 2018-07-01" `
                   "ORDER BY 1 DESC"
