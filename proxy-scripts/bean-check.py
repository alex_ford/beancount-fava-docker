#!/usr/env python3

#import docker
import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("file",
    help="The beancount file to run bean-check on.")
args = parser.parse_args()

completedProcess = subprocess.run(
    [
        "docker",
        "run",
        "-it",
        "--rm",
        "-v", "/c/Users/arf41/Workspaces-Personal/beancount/beancount-data/:/data",
        "--workdir", "/data",
        "arf/beancount-fava-docker",
        "bean-check", args.file
    ],
    check=True)

print(completedProcess)
