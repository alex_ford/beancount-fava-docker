docker : /data/transactions/AllPriorTransactions.beancount:2858:    Cost and price currencies must match: USD 
!= ETH
At C:\Users\arf41\Workspaces-Personal\beancount\beancount-fava-docker\proxy-scripts\bean-check.ps1:8 char:1
+ docker ${RUN_TYPE} ${VOL1} --workdir /data ${CONTAINER_NAME} bean-che ...
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : NotSpecified: (/data/transacti...tch: USD != ETH:String) [], RemoteException
    + FullyQualifiedErrorId : NativeCommandError
 

/data/transactions/AllPriorTransactions.beancount:414:     No position matches 
"Posting(account='Assets:Investments:Coinbase:ETH-Investment-Wallet', units=-0.32950026 ETH, 
cost=CostSpec(number_per=Decimal('312.56'), number_total=None, currency='USD', date=datetime.date(2017, 7, 
12), label=None, merge=False), price=None, flag=None, meta={'lineno': 423, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (0.92063738 ETH {275.59 USD, 
2017-06-25})

   2017-06-28 * "Coinbase" "Transfer 32.59444502 ETH from Coinbase to Cold Storage"
     Assets:Investments:Coinbase:ETH-Investment-Wallet   -3.67536924 ETH {13.80 USD, 2016-10-03} 
     Assets:Investments:Coinbase:ETH-Investment-Wallet   -0.50393764 ETH {40.28 USD, 2017-03-22} 
     Assets:Investments:Coinbase:ETH-Investment-Wallet   -0.55408446 ETH {93.85 USD, 2017-05-10} 
     Assets:Investments:Coinbase:ETH-Investment-Wallet  -10.39078761 ETH {90.07 USD, 2017-05-12} 
     Assets:Investments:Coinbase:ETH-Investment-Wallet  -11.21786782 ETH {90.47 USD, 2017-05-16} 
     Assets:Investments:Coinbase:ETH-Investment-Wallet   -4.64633944 ETH {98.29 USD, 2017-05-18} 
     Assets:Investments:Coinbase:ETH-Investment-Wallet   -0.58554986 ETH {175.89 USD, 2017-05-26}
     Assets:Investments:Coinbase:ETH-Investment-Wallet   -0.69100869 ETH {367.17 USD, 2017-06-14}
     Assets:Investments:Coinbase:ETH-Investment-Wallet   -0.32950026 ETH {312.56 USD, 2017-07-12}
     Expenses:Financial:Network-Fees                      0.00073673 ETH {13.80 USD, 2016-10-03} 
     Assets:Investments:Cold-Storage:Ethereum             3.67463251 ETH {13.80 USD, 2016-10-03} 
     Assets:Investments:Cold-Storage:Ethereum             0.50393764 ETH {40.28 USD, 2017-03-22} 
     Assets:Investments:Cold-Storage:Ethereum             0.55408446 ETH {93.85 USD, 2017-05-10} 
     Assets:Investments:Cold-Storage:Ethereum            10.39078761 ETH {90.07 USD, 2017-05-12} 
     Assets:Investments:Cold-Storage:Ethereum            11.21786782 ETH {90.47 USD, 2017-05-16} 
     Assets:Investments:Cold-Storage:Ethereum             4.64633944 ETH {98.29 USD, 2017-05-18} 
     Assets:Investments:Cold-Storage:Ethereum             0.58554986 ETH {175.89 USD, 2017-05-26}
     Assets:Investments:Cold-Storage:Ethereum             0.69100869 ETH {367.17 USD, 2017-06-14}
     Assets:Investments:Cold-Storage:Ethereum             0.32950026 ETH {312.56 USD, 2017-07-12}


/data/transactions/AllPriorTransactions.beancount:1995:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=0.02637121 BTC, 
cost=CostSpec(number_per=Decimal('3792.01'), number_total=None, currency='USD', date=datetime.date(2017, 9, 
22), label=None, merge=False), price=None, flag=None, meta={'lineno': 1996, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.01118343 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-09-22 * "Coinbase" "Purchased BTC"
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet      0.02637121 BTC {3792.01 USD, 2017-09-22}
     Liabilities:Credit-Cards:Chase-Sapphire-Reserve     -100.00 USD                          


/data/transactions/AllPriorTransactions.beancount:2003:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=0.00819248 BTC, 
cost=CostSpec(number_per=Decimal('6103.16'), number_total=None, currency='USD', date=datetime.date(2017, 10, 
23), label=None, merge=False), price=None, flag=None, meta={'lineno': 2005, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-10-23 * "Coinbase" "Purchase BTC in order to buy XMR on Kraken"
     Liabilities:Credit-Cards:Chase-Sapphire-Reserve      -50.00 USD                          
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet      0.00819248 BTC {6103.16 USD, 2017-10-23}


/data/transactions/AllPriorTransactions.beancount:239:     Not enough lots to reduce "-0.03119906 BTC {2580.00 
USD, 2017-07-12}": 0.02692906 BTC {2580.00 USD, 2017-07-12}

   2017-1
0-24 * "Kraken" "Traded BTC for XMR"
     Assets:Investments:Kraken:Bitcoin  -0.03119906 BTC {2580.00 USD, 2017-07-12}
     Assets:Investments:Kraken:Bitcoin  -0.03198000 BTC {3706.90 USD, 2017-09-23}
     Assets:Investments:Kraken:Bitcoin  -0.00359094 BTC {5904.03 USD, 2017-10-23}
     Expenses:Financial:Commissions         0.00005 BTC {2580.00 USD, 2017-07-12}
     Expenses:Financial:Commissions         0.00341 XMR {87.91 USD, 2017-10-24}  
     Assets:Investments:Kraken:Monero       4.20844 XMR {87.91 USD, 2017-10-24}  


/data/transactions/AllPriorTransactions.beancount:1965:    Not enough lots to reduce "-0.03119906 BTC {2580.00 
USD, 2017-07-12}": 0.02692906 BTC {2580.00 USD, 2017-07-12}

   2017-10-24 * "Kraken" "Trade 0.07105215 BTC for 4.47836508 XMR (Limit Order)"
     kraken-order-id: "OKS34A-X527B-EZSNKM"
     Assets:Investments:Kraken:Monero       4.20844 XMR {159.85 USD, 2017-10-24} 
     Expenses:Financial:Commissions         0.00341 XMR {159.85 USD, 2017-10-24} 
     Expenses:Financial:Commissions      0.00005215 BTC {2580.00 USD, 2017-07-12}
     Assets:Investments:Kraken:Bitcoin  -0.03119906 BTC {2580.00 USD, 2017-07-12}
     Assets:Investments:Kraken:Bitcoin  -0.03198000 BTC {3706.90 USD, 2017-09-23}
     Assets:Investments:Kraken:Bitcoin  -0.00359094 BTC {5904.03 USD, 2017-10-23}


/data/transactions/AllPriorTransactions.beancount:2007:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=-0.00819248 BTC, 
cost=CostSpec(number_per=Decimal('6103.16'), number_total=None, currency='USD', date=datetime.date(2017, 10, 
23), label=None, merge=False), price=None, flag=None, meta={'lineno': 2008, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-10-24 * "Transfer" "Transfer from Coinbase to Kraken"
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet  -0.00819248 BTC {6103.16 USD, 2017-10-23}
     Assets:Investments:Kraken:Bitcoin             0.00787215 BTC {6103.16 USD, 2017-10-23}
     Expenses:Financial:Network-Fees               0.00032033 BTC {6103.16 USD, 2017-10-23}


/data/transactions/AllPriorTransactions.beancount:2012:    No position matches 
"Posting(account='Assets:Investments:GDAX:Bitcoin', units=-0.01141983 BTC, 
cost=CostSpec(number_per=Decimal('4367.43'), number_total=None, currency='USD', date=None, label=None, 
merge=False), price=None, flag=None, meta={'lineno': 2013, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (0.02378809 BTC {4203.78 USD, 
2017-09-27}, 0.03549979 BTC {4225.38 USD, 2017-10-05}, 0.01141983 BTC {4378.35 USD, 2017-08-27})

   2017-10-25 * "Transfer" "Transfer BTC from GDAX to HitBTC 0.04 BTC"
     Assets:Investments:GDAX:Bitcoin    -0.01141983 BTC {4367.43 USD}
     Assets:Investments:GDAX:Bitcoin    -0.02378809 BTC {4193.30 USD}
     Assets:Investments:GDAX:Bitcoin    -0.00479208 BTC {4214.84 USD}
     Assets:Investments:HitBTC:Bitcoin   0.01141983 BTC {4367.43 USD}
     Assets:Investments:HitBTC:Bitcoin   0.02378809 BTC {4193.30 USD}
     Assets:Investments:HitBTC:Bitcoin   0.00479208 BTC {4214.84 USD}


/data/transactions/AllPriorTransactions.beancount:2069:    No position matches 
"Posting(account='Assets:Investments:GDAX:Litecoin', units=-0.01000000 LTC, 
cost=CostSpec(number_per=Decimal('40.85'), number_total=None, currency='USD', date=None, label=None, 
merge=False), price=None, flag=None, meta={'lineno': 2070, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (0.69511756 LTC {40.96 USD, 
2017-06-30}, 0.01 LTC {40.97 USD, 2017-06-30}, 0.01 LTC {41.00 USD, 2017-06-30}, 6.60055736 LTC {41.01 USD, 
2017-06-30}, 0.77371486 LTC {64.62 USD, 2017-08-27})

   2017-10-25 * "Self" "Transfer LTC from GDAX to Cold Storage"
     Assets:Investments:GDAX:Litecoin          -0.01000000 LTC {40.85 USD}
     Assets:Investments:GDAX:Litecoin          -6.60055736 LTC {40.89 USD}
     Assets:Investments:GDAX:Litecoin          -0.69511756 LTC {40.84 USD}
     Assets:Investments:GDAX:Litecoin     
     -0.77371486 LTC {64.43 USD}
     Assets:Investments:GDAX:Litecoin          -0.01000000 LTC {40.88 USD}
     Assets:Investments:Cold-Storage:Litecoin   0.01000000 LTC {40.85 USD}
     Assets:Investments:Cold-Storage:Litecoin   6.60055736 LTC {40.89 USD}
     Assets:Investments:Cold-Storage:Litecoin   0.69511756 LTC {40.84 USD}
     Assets:Investments:Cold-Storage:Litecoin   0.77371486 LTC {64.43 USD}
     Assets:Investments:Cold-Storage:Litecoin   0.01000000 LTC {40.88 USD}


/data/transactions/AllPriorTransactions.beancount:2200:    No position matches 
"Posting(account='Assets:Investments:Bitfinex:IOTA-Position', units=-272.7761745 MI-IOTA, 
cost=CostSpec(number_per=Decimal('0.0001482'), number_total=None, currency='BTC', date=None, label=None, 
merge=False), price=None, flag=None, meta={'lineno': 2201, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (272.77617450 MI-IOTA {0.371992 USD, 
2017-06-15})

   2017-10-29 * "Transfer" "Transfer 272.7 Mi IOTA from Bitfinex to Self-Hosted Investment Wallet"
     Assets:Investments:Bitfinex:IOTA-Position              -272.7761745 MI-IOTA {0.0001482 BTC}
     Assets:Investments:Self-Hosted:IOTA-Investment-Wallet   272.7761745 MI-IOTA {0.0001482 BTC}
       address: "DNAYRPQHXYBQYTIGVAUTQN9SDNFI9FJCITLJBNFYYOHUASLZUYYIWECUURGNSHZMJKBKWVWTKYHEPACZWYP9YCEGKZ"
       txid: "BOKCDDOJXACXBVUTG9GBZOEDZKNMGIHSNOAVIGTM9BRDEC9UY9SSVJZKAFSCZSFZYWBDHFUEYUQAA9999"


/data/transactions/AllPriorTransactions.beancount:2473:    No position matches 
"Posting(account='Assets:Investments:Kraken:Ethereum', units=0.23463 ETH, 
cost=CostSpec(number_per=Decimal('287.005071815'), number_total=None, currency='USD', date=None, label=None, 
merge=False), price=None, flag=None, meta={'lineno': 2476, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.22963 ETH, 0.23487 ETH {287.54 USD, 
2017-11-02})

   2017-11-02 * "Kraken" "Limit order BUY 0.23487804 ETH @ 0.041 BTC/ETH for 0.00963 BTC"
     Assets:Investments:Kraken:Bitcoin   -0.00963 BTC {7000.00 USD}      
     Expenses:Financial:Commissions                                      
     Assets:Investments:Kraken:Ethereum   0.23463 ETH {287.005071815 USD}


/data/transactions/AllPriorTransactions.beancount:2478:    No position matches 
"Posting(account='Assets:Investments:Kraken:Ethereum', units=-0.23463 ETH, 
cost=CostSpec(number_per=Decimal('287.005071815'), number_total=None, currency='USD', date=None, label=None, 
merge=False), price=None, flag=None, meta={'lineno': 2480, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.22963 ETH, 0.23487 ETH {287.54 USD, 
2017-11-02})

   2017-11-02 * "Transfer" "Transfer 0.23463 ETH from Kraken to Cold Storage"
     time: "11:18PM EST"
     Assets:Investments:Kraken:Ethereum        -0.23463 ETH {287.005071815 USD}
     Expenses:Financial:Commissions               0.005 ETH {287.005071815 USD}
     Assets:Investments:Cold-Storage:Ethereum   0.22963 ETH {287.005071815 USD}


/data/transactions/AllPriorTransactions.beancount:3229:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=0.00694847 BTC, 
cost=CostSpec(number_per=Decimal('6909.43'), number_total=None, currency='USD', date=datetime.date(2017, 11, 
14), label=None, merge=False), price=None, flag=None, meta={'lineno': 3232, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-11-14 * "Coinbase" "Buy BTC"
     time: "22:09 EST"
     Liabilities:Credit-Cards:Chase-Sapphire-Reserve      -50.00 USD                          
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet      0.00694847 BTC {6909.43 USD, 2017-11-14}
     Expenses:Financial:Commissions                         1.99 USD                          


/data/transactions/AllPriorTransactions.beancount:3235:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=-0.00694847 BTC, 
cost=CostSpec(number_per=Decimal('6909.43'), number_total=No
ne, currency='USD', date=datetime.date(2017, 11, 14), label=None, merge=False), price=None, flag=None, 
meta={'lineno': 3237, 'filename': '/data/transactions/AllPriorTransactions.beancount'})" against balance 
(-0.04351074 BTC, 0.01278004 BTC {2894.36 USD, 2017-08-01})

   2017-11-14 * "Transfer" "Transfer BTC from Coinbase to GDAX"
     time: "22:11 EST"
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet  -0.00694847 BTC {6909.43 USD, 2017-11-14}
     Assets:Investments:GDAX:Bitcoin               0.00694847 BTC {6909.43 USD, 2017-11-14}


/data/transactions/AllPriorTransactions.beancount:3322:    No position matches 
"Posting(account='Assets:Investments:GDAX:Bitcoin', units=-0.00694847 BTC, 
cost=CostSpec(number_per=Decimal('6909.43'), number_total=None, currency='USD', date=datetime.date(2017, 11, 
14), label=None, merge=False), price=None, flag=None, meta={'lineno': 3324, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (0.03979452 BTC {7305.00 USD, 
2017-11-04}, 0.00020548 BTC {7394.95 USD, 2017-11-04})

   2017-11-16 * "GDAX" "Limit order SELL 0.04822248 BTC @ 7550.00 USD/BTC for 364.079724 USD"
     Assets:Investments:GDAX:Bitcoin   -0.04127401 BTC {7400.00 USD, 2017-11-08}
     Assets:Investments:GDAX:Bitcoin   -0.00694847 BTC {6909.43 USD, 2017-11-14}
     Income:Investments:Capital-Gains       -10.64 USD                          
     Assets:Investments:GDAX:USD        364.079724 USD                          


/data/transactions/AllPriorTransactions.beancount:3503:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=0.05978697 BTC, 
cost=CostSpec(number_per=Decimal('8363.03'), number_total=None, currency='USD', date=datetime.date(2017, 11, 
19), label=None, merge=False), price=None, flag=None, meta={'lineno': 3504, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-11-19 * "Coinbase" "Bought 0.05978697 BTC at 8042.22 USD/BTC for 500.00 USD (19.18 USD fee) using CSR 
credit card"
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet      0.05978697 BTC {8363.03 USD, 2017-11-19}
     Liabilities:Credit-Cards:Chase-Sapphire-Reserve     -500.00 USD                          


/data/transactions/AllPriorTransactions.beancount:3508:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=-0.05978697 BTC, 
cost=CostSpec(number_per=Decimal('8363.03'), number_total=None, currency='USD', date=datetime.date(2017, 11, 
19), label=None, merge=False), price=None, flag=None, meta={'lineno': 3509, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-11-20 * "Transfer" "Transfered 0.05978697 BTC from Coinbase to GDAX"
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet  -0.05978697 BTC {8363.03 USD, 2017-11-19}
     Assets:Investments:GDAX:Bitcoin               0.05978697 BTC {8363.03 USD, 2017-11-19}


/data/transactions/AllPriorTransactions.beancount:4188:    No position matches 
"Posting(account='Assets:Investments:GDAX:Bitcoin', units=-0.05978697 BTC, 
cost=CostSpec(number_per=Decimal('8363.03'), number_total=None, currency='USD', date=datetime.date(2017, 11, 
19), label=None, merge=False), price=None, flag=None, meta={'lineno': 4190, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (0.03979452 BTC {7305.00 USD, 
2017-11-04}, 0.00020548 BTC {7394.95 USD, 2017-11-04}, 0.04127401 BTC {7400.00 USD, 2017-11-08}, 0.03822400 
BTC {9600.00 USD, 2017-11-27})

   2017-11-28 * "GDAX" "Sell CC Margin 0.05978697 BTC @ 10,100.00 USD/BTC for 603.85 USD"
     time: "21:53 EST"
     Assets:Investments:GDAX:Bitcoin       -0.05978697 BTC {8363.03 USD, 2017-11-19}
     Income:Investments:Capital-Gains  -103.8497762809 USD                          
     Assets:Investments:GDAX:USD                603.85 USD                          


/data/transactions/AllPriorTransactions.beancount:4539:    No position matches "Posting(account='
Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=0.01619131 BTC, 
cost=CostSpec(number_per=Decimal('15440.38'), number_total=None, currency='USD', date=datetime.date(2017, 12, 
9), label=None, merge=False), price=None, flag=None, meta={'lineno': 4541, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-12-09 * "Coinbase" "Purchased 0.01619131 BTC CC Margin at 15440.38 USD/BTC for 250.00 USD" #cc-margin
     Liabilities:Credit-Cards:Chase-Sapphire-Reserve     -250.00 USD                           
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet      0.01619131 BTC {15440.38 USD, 2017-12-09}



/data/transactions/AllPriorTransactions.beancount:4615:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=-0.01619131 BTC, 
cost=CostSpec(number_per=Decimal('15440.38'), number_total=None, currency='USD', date=datetime.date(2017, 12, 
9), label=None, merge=False), price=None, flag=None, meta={'lineno': 4616, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-12-09 * "Transfer" "Transfer 0.01619131 BTC from Coinbase to GDAX"
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet  -0.01619131 BTC {15440.38 USD, 2017-12-09}
     Assets:Investments:GDAX:Bitcoin               0.01619131 BTC {15440.38 USD, 2017-12-09}


/data/transactions/AllPriorTransactions.beancount:4648:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=0.00570738 BTC, 
cost=CostSpec(number_per=Decimal('17521.17'), number_total=None, currency='USD', date=datetime.date(2017, 12, 
14), label=None, merge=False), price=None, flag=None, meta={'lineno': 4649, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-12-14 * "Coinbase" "Purchase 0.00570738 BTC on CC Margin at 17521.17 USD/BTC for 100.00 USD" #cc-margin
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet      0.00570738 BTC {17521.17 USD, 2017-12-14}
     Liabilities:Credit-Cards:Chase-Sapphire-Reserve     -100.00 USD                           


/data/transactions/AllPriorTransactions.beancount:4660:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=-0.00112296 BTC, 
cost=CostSpec(number_per=Decimal('2785.0438'), number_total=None, currency='USD', date=datetime.date(2017, 6, 
7), label=None, merge=False), price=None, flag=None, meta={'lineno': 4661, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-12-14 * "ADJUSTMENT" "ADJUSTMENT (REMOVE ME)"
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet  -0.00112296 BTC {2785.0438, 2017-06-07}
     Equity:Opening-Balances                                                             


/data/transactions/AllPriorTransactions.beancount:4711:    No position matches 
"Posting(account='Assets:Investments:GDAX:Bitcoin', units=-0.01485241 BTC, 
cost=CostSpec(number_per=Decimal('15440.38'), number_total=None, currency='USD', date=datetime.date(2017, 12, 
9), label=None, merge=False), price=None, flag=None, meta={'lineno': 4713, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (0.03979452 BTC {7305.00 USD, 
2017-11-04}, 0.00020548 BTC {7394.95 USD, 2017-11-04}, 0.04127401 BTC {7400.00 USD, 2017-11-08}, 0.03822400 
BTC {9600.00 USD, 2017-11-27}, 0.00617314 BTC {16199.21 USD, 2017-12-08}, 0.00599150 BTC {16690.31 USD, 
2017-12-14})

   2017-12-16 * "GDAX" "Sold 0.01485241 BTC of CC Margin at 18500.00 USD/BTC for 274.77 USD" #cc-margin
     time: "13:21:18 UTC"
     Assets:Investments:GDAX:Bitcoin      -0.01485241 BTC {15440.38 USD, 2017-12-09}
     Income:Investments:Capital-Gains  -45.4427306842 USD                           
     Assets:Investments:GDAX:USD           274.769585 USD                           


/data/transactions/AllPriorTransactions.beancount:4728:    No position matches 
"Posting(account='Assets:Investments:GDAX:Bitcoin', units=-0.00133890 BTC, 
cost=CostSpec(number_per=Decimal('15440.38'), number_total=None, currency='USD', date=datetime.date(2017, 12, 
9), label=None, merge=False), price=None, flag=None, meta={'lineno': 4731, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (0.03979452 BTC {7305.00 USD, 
2017-11-04}, 0.00020548 BTC {7394.95 USD, 2017-11-04}, 0.04127401 BTC {7400.00 USD, 2017-11-08}, 0.00599150 
BTC {16690.31 USD, 2017-12-14})

   2017-12-16 * "Transfer" "Transfer 0.05172754 BTC From GDAX to BTC Cold Storage"
     Assets:Investments:GDAX:Bitcoin          -0.03822400 BTC {9
600.00 USD, 2017-11-27} 
     Assets:Investments:GDAX:Bitcoin          -0.00617314 BTC {16199.21 USD, 2017-12-08}
     Assets:Investments:GDAX:Bitcoin          -0.00133890 BTC {15440.38 USD, 2017-12-09}
     Assets:Investments:GDAX:Bitcoin          -0.00599150 BTC {16690.31 USD, 2017-12-14}
     Assets:Investments:Cold-Storage:Bitcoin   0.03822400 BTC {9600.00 USD, 2017-11-27} 
     Assets:Investments:Cold-Storage:Bitcoin   0.00617314 BTC {16199.21 USD, 2017-12-08}
     Assets:Investments:Cold-Storage:Bitcoin   0.00133890 BTC {15440.38 USD, 2017-12-09}
     Assets:Investments:Cold-Storage:Bitcoin   0.00599150 BTC {16690.31 USD, 2017-12-14}


/data/transactions/AllPriorTransactions.beancount:4797:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=0.02581910 BTC, 
cost=CostSpec(number_per=Decimal('19365.51'), number_total=None, currency='USD', date=datetime.date(2017, 12, 
18), label=None, merge=False), price=None, flag=None, meta={'lineno': 4800, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-12-18 * "Coinbase" "Purchased 0.0258191 BTC on CC Margin at 19365.51 USD/BTC for 500.00 USD" #cc-margin
     cc-margin: "true"
     time: "13:05 EST"
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet      0.02581910 BTC {19365.51 USD, 2017-12-18}
     Liabilities:Credit-Cards:Chase-Sapphire-Reserve     -500.00 USD                           


/data/transactions/AllPriorTransactions.beancount:4803:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=-0.0258191 BTC, 
cost=CostSpec(number_per=Decimal('19365.51'), number_total=None, currency='USD', date=datetime.date(2017, 12, 
18), label=None, merge=False), price=None, flag=None, meta={'lineno': 4804, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-12-18 * "Transfer" "Transfer from BTC Spending Wallet to BTC CC Margin Wallet on Coinbase" #cc-margin
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet           -0.0258191 BTC {19365.51 USD, 2017-12-18}
     Assets:Investments:Coinbase:BTC-CC-Margin-Investment   0.0258191 BTC {19365.51 USD, 2017-12-18}


/data/transactions/AllPriorTransactions.beancount:4807:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=-0.00570738 BTC, 
cost=CostSpec(number_per=Decimal('17521.17'), number_total=None, currency='USD', date=datetime.date(2017, 12, 
14), label=None, merge=False), price=None, flag=None, meta={'lineno': 4808, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-12-18 * "Transfer" "Transfer from BTC Spending Wallet to BTC CC Margin Wallet on Coinbase" #cc-margin
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet           -0.00570738 BTC {17521.17 USD, 2017-12-14}
     Assets:Investments:Coinbase:BTC-CC-Margin-Investment   0.00570738 BTC {17521.17 USD, 2017-12-14}


/data/transactions/AllPriorTransactions.beancount:4816:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spending-Wallet', units=0.02614363 BTC, 
cost=CostSpec(number_per=Decimal('19125.12'), number_total=None, currency='USD', date=datetime.date(2017, 12, 
19), label=None, merge=False), price=None, flag=None, meta={'lineno': 4819, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (-0.04351074 BTC, 0.01278004 BTC 
{2894.36 USD, 2017-08-01})

   2017-12-19 * "Coinbase" "Purchased 0.02614363 BTC on CC Margin at 19125.12 USD/BTC for 500.00 USD" 
#cc-margin
     cc-margin: "true"
     time: "08:15 EST"
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet      0.02614363 BTC {19125.12 USD, 2017-12-19}
     Liabilities:Credit-Cards:Chase-Sapphire-Reserve     -500.00 USD                           


/data/transactions/AllPriorTransactions.beancount:4822:    No position matches 
"Posting(account='Assets:Cryptos:Coinbase:BTC-Spend
ing-Wallet', units=-0.02614363 BTC, cost=CostSpec(number_per=Decimal('19125.12'), number_total=None, 
currency='USD', date=datetime.date(2017, 12, 19), label=None, merge=False), price=None, flag=None, 
meta={'lineno': 4824, 'filename': '/data/transactions/AllPriorTransactions.beancount'})" against balance 
(-0.04351074 BTC, 0.01278004 BTC {2894.36 USD, 2017-08-01})

   2017-12-19 * "Transfer" "Transfer from BTC Spending Wallet to BTC CC Margin Wallet on Coinbase" #cc-margin
     time: "08:25 EST"
     Assets:Cryptos:Coinbase:BTC-Spending-Wallet           -0.02614363 BTC {19125.12 USD, 2017-12-19}
     Assets:Investments:Coinbase:BTC-CC-Margin-Investment   0.02614363 BTC {19125.12 USD, 2017-12-19}



/data/transactions/AllPriorTransactions.beancount:5328:    No position matches 
"Posting(account='Assets:Investments:Coinbase:BTC-CC-Margin-Investment', units=-0.00570738 BTC, 
cost=CostSpec(number_per=Decimal('17521.17'), number_total=None, currency='USD', date=datetime.date(2017, 12, 
14), label=None, merge=False), price=None, flag=None, meta={'lineno': 5329, 'filename': 
'/data/transactions/AllPriorTransactions.beancount'})" against balance (0.01329165 BTC {15047.04 USD, 
2017-12-22}, 0.01428574 BTC {17499.97 USD, 2017-12-19}, 0.01394995 BTC {17921.21 USD, 2017-12-19}, 0.01346176 
BTC {18571.12 USD, 2017-12-19}, 0.0179541 BTC {19494.15 USD, 2017-12-17})

   2018-01-03 * "Transfer" "Transfer BTC from Coinbase to GDAX"
     Assets:Investments:Coinbase:BTC-CC-Margin-Investment  -0.00570738 BTC {17521.17 USD, 2017-12-14}
     Assets:Investments:GDAX:Bitcoin                        0.00570738 BTC {17521.17 USD, 2017-12-14}


/data/transactions/2018/201808.beancount:369:     Balance failed for 'Assets:PayPal': expected 822.47 USD != 
accumulated 384.14 USD (438.33 too little)

   2018-08-14 balance Assets:PayPal                                   822.47 USD


/data/transactions/2018/201808.beancount:669:     Balance failed for 'Assets:Investments:GroundFloor:Cash': 
expected 0.82 USD != accumulated 50.82 USD (50.00 too much)

   2018-08-30 balance Assets:Investments:GroundFloor:Cash             0.82 USD


/data/transactions/2018/201809.beancount:39:      Balance failed for 'Assets:Investments:Fidelity-401k': 
expected 62066.96 USD != accumulated 48934.46 USD (13132.50 too little)

   2018-09-05 balance Assets:Investments:Fidelity-401k                62066.96 USD


/data/transactions/2018/201807.beancount:781:     Invalid reference to unknown account 'Expenses:PayPal'

   2018-07-23 * "Transfer" "Ally Checking to PayPal"
     generated: "MIXED"
     paypal-txid: "79H141049V4713804"
     statement-desc: "Bank Deposit to PP Account "
     time: "17:55:22 EDT"
     Assets:Banking:Ally:Checking  -145.19 USD
     Expenses:PayPal        145.19 USD


/data/transactions/2018/201810.beancount:218:     Invalid reference to unknown account 
'Equity:Trading:Lot-Scrubbing'

   2018-10-10 ! "BestMixer.io" "Tumble Bitcoins from GDAX to DNM" #DNM
     bestmixer-address: "3Aynzu1cLSuG8nspPNRT6jdMW9VDay6npg"
     bestmixer-code: "1ZzLtuG4b9"
     bestmixer-url: "https://bestmixer.io/en/t/2BDZekEQ95"
     dnm-destination: "3831eqYtnb4SoGMxJXyJFa5F6nWTZfyR2R"
     Assets:Investments:GDAX:BTC   -0.01868816 BTC {6668.72 USD, 2018-09-27}
     Assets:Investments:GDAX:BTC   -0.01506508 BTC {6618.01 USD, 2018-09-30}
     Equity:Trading:Lot-Scrubbing   0.01868816 BTC {6668.72 USD, 2018-09-27}
     Equity:Trading:Lot-Scrubbing   0.01506508 BTC {6618.01 USD, 2018-09-30}
     Equity:Trading:Lot-Scrubbing   0.01868816 BTC                          
     Equity:Trading:Lot-Scrubbing   0.01506508 BTC                          
     Expenses:Shopping:Other          0.033753 BTC                          


/data/transactions/2018/201810.beancount:218:     Invalid reference to unknown account 
'Assets:Investments:GDAX:BTC'

   2018-10-10 ! "BestMixer.io" "Tumble Bitcoins from GDAX to DNM" #DNM
     bestmixer-address: "3Aynzu1cLSuG8nspPNRT6jdMW9VDay6npg"
     bestmixer-code: "1ZzLtuG4b9"
     bestmixer-url: "https://bestmixer.io/en/t/2BDZekEQ95"
     dnm-destination: "3831eqYtnb4SoGMxJXyJFa5F6nWTZfyR2R"
     Assets:Investments:GDAX:BTC   -0.01868816 BTC {6668.72 USD, 2018-09-27}
     Assets:Investments:GDAX:BTC   -0.01506508 BTC {6618.01 USD, 2018-09-30}
     Equity:Trading:Lot-Scrubbing   0.01868816 BTC {6668.72 USD, 2018-09-27}
     Equity:Trading:Lot-Scrubbing   0.01506508 BTC {6618.01 USD, 2018-09-30}
     Equity:Trading:Lot-Scrubbing   0.01868816 BTC                          
     Equity:Trading:Lot-Scrubbing   0.01506508 BTC                          
     Expenses:Shopping:Other          0.033753 BTC                          


/data/transactions/2018/201810.beancount:218:     Invalid currency BTC for account 'Expenses:Shopping:Other'

   2018-10-10 ! "BestMixer.io" "Tumble Bi
tcoins from GDAX to DNM" #DNM
     bestmixer-address: "3Aynzu1cLSuG8nspPNRT6jdMW9VDay6npg"
     bestmixer-code: "1ZzLtuG4b9"
     bestmixer-url: "https://bestmixer.io/en/t/2BDZekEQ95"
     dnm-destination: "3831eqYtnb4SoGMxJXyJFa5F6nWTZfyR2R"
     Assets:Investments:GDAX:BTC   -0.01868816 BTC {6668.72 USD, 2018-09-27}
     Assets:Investments:GDAX:BTC   -0.01506508 BTC {6618.01 USD, 2018-09-30}
     Equity:Trading:Lot-Scrubbing   0.01868816 BTC {6668.72 USD, 2018-09-27}
     Equity:Trading:Lot-Scrubbing   0.01506508 BTC {6618.01 USD, 2018-09-30}
     Equity:Trading:Lot-Scrubbing   0.01868816 BTC                          
     Equity:Trading:Lot-Scrubbing   0.01506508 BTC                          
     Expenses:Shopping:Other          0.033753 BTC                          


/data/transactions/2018/201810.beancount:218:     Transaction does not balance: (0.06750624 BTC)

   2018-10-10 ! "BestMixer.io" "Tumble Bitcoins from GDAX to DNM" #DNM
     bestmixer-address: "3Aynzu1cLSuG8nspPNRT6jdMW9VDay6npg"
     bestmixer-code: "1ZzLtuG4b9"
     bestmixer-url: "https://bestmixer.io/en/t/2BDZekEQ95"
     dnm-destination: "3831eqYtnb4SoGMxJXyJFa5F6nWTZfyR2R"
     Assets:Investments:GDAX:BTC   -0.01868816 BTC {6668.72 USD, 2018-09-27}
     Assets:Investments:GDAX:BTC   -0.01506508 BTC {6618.01 USD, 2018-09-30}
     Equity:Trading:Lot-Scrubbing   0.01868816 BTC {6668.72 USD, 2018-09-27}
     Equity:Trading:Lot-Scrubbing   0.01506508 BTC {6618.01 USD, 2018-09-30}
     Equity:Trading:Lot-Scrubbing   0.01868816 BTC                          
     Equity:Trading:Lot-Scrubbing   0.01506508 BTC                          
     Expenses:Shopping:Other          0.033753 BTC                          


INFO    : Operation: 'beancount.loader (total)'                       Time:   1258 ms

