# Powershell script which simply connects you to a shell inside the Docker container
# running Beancount (and Fava). From inside, you have access to all of the command
# line utilities provided by the Beancount project:
#   bean-bake      bean-example   bean-format    bean-query     bean-web
#   bean-check     bean-extract   bean-identify  bean-report
#   bean-doctor    bean-file      bean-price     bean-sq
#
# @author Alex Ford (arf4188@gmail.com)

# TODO: provide a "-c" switch that accepts a command to run instead of opening an interactive shell!

Write-Output "[ INFO ] Script root is: ${PSScriptRoot}"

. ${PSScriptRoot}\config.ps1

docker ${RUN_TYPE} ${VOL1} -it --workdir /data ${CONTAINER_NAME} bash
