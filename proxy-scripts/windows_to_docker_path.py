#!/bin/usr python3

# Script which helps take a canonical Windows path and converts it to a
# valid Docker container path.

import os
import shutil

#WINDOWS_MOUNT_PATH = "C:\Users\arf41\Workspaces-Personal\beancount\beancount-data\"
WINDOWS_MOUNT_PATH = "/c/Users/arf41/Workspaces-Personal/beancount/beancount-data/"
DOCKER_CONTAINER_PATH = "/data"
CURRENT_WORKING_DIR = os.getcwd()


def windows_to_docker_path(windowsPath: str) -> str:
    """ 
    Converts the specified 'windowsPath' to the Docker path equivalent.

    This involves using the current working directory and computing the relation
    between that, the WINDOWS_MOUNT_PATH and the DOCKER_CONTAINER_PATH, as
    defined by how the Docker container is mounting the Windows volume.

    For example, if ran from *this* directory (where this script is), we would
    get something like this:
        windows_to_docker_path("..\\..\\beancount-data\\transactions\\2018\\201811.beancount")
    OR
        windows_to_docker_path("201811.beancount")
    In both cases, this is what is returned:
        /data/transactions/2018/201811.beancount
    """
    raise RuntimeError("Not Yet Implemented")


def resolve_beancount_file(filename: str) -> str:
    retval = "/data/transactions"
    yearPart = filename[0:4]
    monthPart = filename[4:6]
    retval = "{}/{}/{}".format(retval, yearPart, filename)
    return retval
