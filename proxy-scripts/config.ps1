# Configuration File for the proxy scripts that help bridge Beancount/Fava running inside
# a Docker container with the user's native terminal environment.

$CONTAINER_NAME = "beancount-fava-docker"

# Selection: "run" or "exec"
$RUN_TYPE = "run"
# Specify or clear these vars as desired
$RUN_IT = "-it"
$RUN_RM = "--rm"
# Specify or clear the absPath:/data/
$VOL1 = "-v/c/Users/arf41/Workspaces-Personal/beancount/beancount-data:/data/" 
# Ports
$PORTS = "5000:5000"
# Primary beancount file to use
$BEANCOUNT_FILE = "main.beancount"
