#!/usr/env python3

# TODO: consider using the Python Docker module for first-class access to Docker!
# This would replace the calls to docker via subprocess.run()
#import docker
import subprocess
import argparse
import os
from datetime import datetime

import windows_to_docker_path as Win2DockPath

import logging
logging.basicConfig(level=logging.INFO)
_log = logging.getLogger(__name__)

parser = argparse.ArgumentParser(
    description="Provides a proxy to the Dockerized 'bean-format' tool."
)
parser.add_argument("-f", "--file",
                    help="The beancount file to run bean-format on. Only "
                         "specify the final part, i.e. the filename, as the "
                         "directory structure is managed by this/other scripts.")
args = parser.parse_args()

BEANCOUNT_DATA_DIR = os.path.realpath(
    os.path.expanduser("~/Workspaces-Personal/beancount/beancount-data"))
_log.info("BEANCOUNT_DATA_DIR is: {}".format(BEANCOUNT_DATA_DIR))
assert os.path.exists(BEANCOUNT_DATA_DIR)

# TODO: the above still won't be quite right for "Docker on Windows"
# We will probably get something like: "C:\Users\arf41\Workspaces-Personal\beancount\beancount-data\"
# when we really want: /c/Users/arf41/Workspaces-Personal/beancount/beancount-data/
# That is what worked before...

# This could be switched out for any of the various Beancount tools
CONTAINER_BEANCOUNT_EXE = "bean-format"

# TODO: have this either get set from the CLI args
if args.file is not None:
    YEAR = args.file[0:4]
    MONTH = args.file[4:6]
    CONTAINER_BEANCOUNT_FILE = "/data/transactions/{}/{}".format(YEAR, args.file)
else:
    CURRENT_MONTH = "{:02}".format(datetime.now().month)
    CURRENT_YEAR = "{}".format(datetime.now().year)
    CONTAINER_BEANCOUNT_FILE = "/data/transactions/{}/{}{}.beancount".format(
        CURRENT_YEAR,
        CURRENT_YEAR,
        CURRENT_MONTH)

_log.info("CONTAINER_BEANCOUNT_FILE is: {}".format(CONTAINER_BEANCOUNT_FILE))

BEANCOUNT_TOOL_ARGS = [
    CONTAINER_BEANCOUNT_FILE,
    "-w", "70",
    "-o", CONTAINER_BEANCOUNT_FILE
]
_log.info("BEANCOUNT_TOOL_ARGS is: {}".format(BEANCOUNT_TOOL_ARGS))

# Now let's create the full Docker command with all the args we need
SUBPROCESS_RUN_COMMAND = [
        "docker",
        "run",
        "-it",
        "--rm",
        "-v", "{}:/data".format(BEANCOUNT_DATA_DIR),
        "--workdir", "/data",
        "arf/beancount-fava-docker",
        CONTAINER_BEANCOUNT_EXE
    ]
SUBPROCESS_RUN_COMMAND.extend(BEANCOUNT_TOOL_ARGS)
_log.info("SUBPROCESS_RUN_COMMAND is: {}".format(SUBPROCESS_RUN_COMMAND))

# Run it!
completedProcess = subprocess.run(
    SUBPROCESS_RUN_COMMAND,
    check=True)

_log.info(completedProcess)
exit(completedProcess.returncode)
