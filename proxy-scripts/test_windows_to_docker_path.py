from windows_to_docker_path import resolve_beancount_file

import pytest
import logging
logging.basicConfig(level=logging.INFO)
_log = logging.getLogger(__name__)


def test_resolve_beancount_file():
    testInputValues = [
        {
            "filename": "201811.beancount",
            "expected": "/data/transactions/2018/201811.beancount"
        },
        {
            "filename": "201812.beancount",
            "expected": "/data/transactions/2018/201812.beancount"
        },
        {
            "filename": "201901.beancount",
            "expected": "/data/transactions/2019/201901.beancount"
        },
    ]
    for testInput in testInputValues:
        retval = resolve_beancount_file(testInput["filename"])
        _log.info("Resolved {} to --> {}".format(testInput["filename"],
                                                 testInput["expected"]))
        assert retval == testInput["expected"]
