REM This script will run beancount/fava using Docker.

docker run --rm -p 5000:5000 ^
    -v c:/Users/arf41/beancountData:/data ^
    -e BEANCOUNT_INPUT_FILE=/data/ledger.beancount ^
    yegle/fava
