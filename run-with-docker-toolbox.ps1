# Windows PowerShell script for using Docker Toolbox to launch a Dockerized version of
# the Beancount and Fava software. With this, you have a complete environment inside a
# Docker container that can support beancount/fava development, usage, testing, etc.
#
# This leverages the existing Dockerfile created/maintained by yegle/fava on Docker Hub.
#
# Script Maturity: Alpha - usable, but likely has bugs, major limitations, missing features...
#
# @author Alex Richard Ford (arf4188@gmail.com)

# FEATURES
# [x] Helps manage the Docker workflow on Windows so that it's a bit less cumbersome.
# [x] Starts the Docker container for yegle/fava
# [x] Provides the correct Host-based URL to reduce confusion from Docker's base output
# [x] Supports pre and post hook scripts for syncing data file in user's custom way

# TODO allow use of other docker-machines than 'default' via argument (e.g. --machine notDefault )

# Push the script's root directory so we can run this from anywhere with the same result
Push-Location $PSScriptRoot

# Execute Pre-hook Script
& .\run-hook-pre.ps1
# TODO need to do error handling here, for example:
# If (-Not $? == 0) {
#     Write-Output "[ ERROR ] Something went wrong with the pre-hook. Please correct before trying again."
#     Exit -1
# }
# Exit 0

# Start the Docker Machine VM - this fails gracefully, so no need to wrap it
Write-Output "[ INFO ] Starting default Docker machine..."
docker-machine start default

# Load the Docker environment
# This can be run idempotently, so no need to guard it from previous runs
Write-Output "[ INFO ] Sourcing default Docker machine environment..."
& docker-machine env default | Invoke-Expression

# Grab the Docker Machine IP
Write-Output "[ INFO ] Acquiring default Docker machine IP..."
$dmip = docker-machine ip default
Write-Output "Determined IP to be: ${dmip}"

# Start the Fava container if needed
# TODO provide script argument that allows to run 'attached' instead of 'detached' (-d)
# TODO need to add check to only do this if needed
Write-Output "[ INFO ] Starting the Fava Docker container..."
$cid = docker ps -q --filter name="beancount-fava-docker"
if ($cid) {
    Write-Host "[ WARN ] A container with the name Fava is already running, can't start a second one."
    Write-Host "[ WARN ] If desired, stop the existing Fava container with the stop.ps1 script."
} else {
    docker run --rm -p 5000:5000 -d `
        -v /c/Users/arf41/Workspaces-Personal/beancount/beancount-data:/data `
        -e BEANCOUNT_INPUT_FILE=/data/main.beancount `
        --name beancount-fava-docker `
        arf/beancount-fava-docker
    # Sleep for a few seconds
    Write-Output "Sleeping for 7 seconds while Fava container starts!"
    Start-Sleep 7
}

# Report Host usable URL
Write-Output "[ INFO ] Fava can be accessed from your Windows host here: http://${dmip}:5000"

# Now launch Chrome in "pseudo" app mode
# TODO grab the URL/PORT dynamically in case it's different on other machines
# TODO support using the 'default' browser, and specifying browser (e.g. --browser "name")
& "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --app="http://${dmip}:5000/beancount/income_statement/?interval=week&time=day-60+-+day"

# Post hook
# TODO this currently doesn't wait for the user to close Fava... should it? Probably.
# OR, consider starting a sync daemon! Would be cool if it could run while Fava was open, and stops when Fava closes.
# Additionally, it would issue the sync command when it detects a file change has finished saving to disk.
& .\run-hook-post.ps1

# Pop the direction and return user to where they were
Pop-Location
