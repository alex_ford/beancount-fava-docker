# Beancount Fava Docker (beancount-fava-docker)

This micro-project builds off of the Beancount double-entry accounting CLI project. <http://furius.ca/beancount/>

This makes use of yegle/fava's public Docker image, combined with some scripts and other modifications to help improve working with it. By leveraging Docker, you can deploy a "local" copy of the Beancount/Fava application anywhere!

**Author**: Alex Ford (arf4188@gmail.com)

**License**: MIT License (see [LICENSE](LICENSE) file for more details)

## Dependencies

In addition to this Git repo, you will also need the following other repos:

* None

## Quick Start Usage

1. Download and install the latest version of Docker
1. Now select your variant from the enumerations below:
* On Windows - Docker Toolbox
  * Using Docker Quickstart Terminal
    1. Launch the Docker Quickstart Terminal (denoted as ``dst>``) which should automatically download and install the Docker VM into VirtualBox.
    1. Run ``dst> .\runDockerToolbox.sh`` to download the yegle/fava Docker image, start a container of it, and launch the Chrome web browser pointed to the correct URL
  * Using PowerShell
    1. ...
* On Windows - Docker for Windows
  1. ...
* On Linux
  1. ...

## Common Conventions

In all of my beancount-* repos, I follow the following conventions:

* Beancount data files (with your actual transactions in them) use the .bcdat extension
* Beancount spec files (which are supporting files used by my scripts and plugins) use the .bcspec extension
  
## "Beanshell"

This is a moniker for a shell started within the Beancount/Fava Docker container. Simply execute the following script:

```powershell
.\bean-shell.ps1
```

The container definition should premount your `beancount-data` directory to `/data` and the script should drop you right into it. From there, you have access to all of the command line tools that are part of the Beancount project:

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| bean-bake   | bean-example | bean-format   | bean-query  | bean-web |
| bean-check  | bean-extract | bean-identify | bean-report |          |
| bean-doctor | bean-file    | bean-price    | bean-sq     |          |

You can use the `-h` switch on any of the command line tools to get more information about them:

```sh
bean-format -h
```

## Sample Operations

### Backup and format the data file

```sh
cp ledger.beancount ~ledger.beancount && bean-format -o ledger.beancount ledger.beancount
```

I like to keep one backup of my data file before preforming most operations. You never know when you might want to hit undo! `bean-format` simply makes the content of the data file look nicer.