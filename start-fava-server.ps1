# Start the default docker-machine if it's not already started
docker-machine start

sleep 2

# Source default docker-machine environment
& docker-machine env | Invoke-Expression

Write-Output "[ INFO ] Acquiring default Docker machine IP..."
$DOCKER_MACHINE_IP = docker-machine ip default
Write-Output "Determined IP to be: ${dmip}"

# TODO: convert to Python script so it can work cross-platform
docker run --rm -d `
    -p 5000:5000 `
    -v/c/Users/arf41/Workspaces-Personal/beancount/beancount-data:/data `
    --workdir /data `
    -e BEANCOUNT_INPUT_FILE=/data/main.beancount `
    --name beancount-fava-container `
    arf/beancount-fava-docker

sleep 4

Write-Output "[ INFO ] Fava should now be available at: http://${DOCKER_MACHINE_IP}:5000"
Write-Output "[ INFO ] You can also use the .\launch-fava-chrome-webapp.ps1 script."
