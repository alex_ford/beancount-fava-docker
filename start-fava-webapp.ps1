# This script attempts to launch Fava in the defined browser below.
# @author Alex Ford (arf4188@gmail.com)

$chrome = "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
$favaHost = docker-machine ip default

& ${chrome} --start-maximized --app=http://${FavaHost}:5000