# This is my own Dockerfile for custom needs. This started as a copy of
# the yegle/fava Dockerfile, then mods were added as needed.

# Starting with Bitnami's minideb, it should produce a smaller image size
FROM bitnami/minideb:latest

ENV BEANCOUNT_INPUT_FILE "/beancount/beancount-data/main.beancount"
ENV FAVA_OPTIONS "-H 0.0.0.0"
ENV FINGERPRINT "3f:d3:c5:17:23:3c:cd:f5:2d:17:76:06:93:7e:ee:97:42:21:14:aa"
ENV BUILDDEPS "libxml2-dev libxslt-dev gcc musl-dev mercurial git nodejs make g++ python3-dev"
ENV RUNDEPS "apt-utils libxml2 xsltproc python3 python3-pip python3-setuptools"
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Minideb offers the install_package wrapper around apt
RUN cd /root \
        && install_packages ${BUILDDEPS} ${RUNDEPS} \
        && python3 -m pip install beancount fava
# TODO: add cleanup steps

## The following is a reference from yegel/fava's Dockerfile:
# RUN cd /root \
#         && apt-get update \
#         && apt-get install -y $BUILDDEPS $RUNDEPS \
#         && curl -sL https://deb.nodesource.com/setup_10.x | bash - \
#         && apt-get install -y nodejs \
#         && hg clone https://bitbucket.org/blais/beancount \
#         && (cd beancount && hg log -l1) \
#         && rm -rf ./beancount/.hg \
#         && python3 -m pip install ./beancount \
#         && git clone https://github.com/beancount/fava.git \
#         && (cd fava && git log -1) \
#         && make -C fava \
#         && make -C fava mostlyclean \
#         && rm fava/CHANGES \
#         && python3 -m pip install ./fava \
#         && python3 -m pip uninstall --yes pip \
#         && find /usr/local/lib/python3.?/site-packages -name *.so -print0|xargs -0 strip -v \
#         && apt-get purge $BUILDDEPS \
#         && rm -rf /tmp /root \
#         && find /usr/local/lib/python3.? -name __pycache__ -print0|xargs -0 rm -rf \
#         && find /usr/local/lib/python3.? -name *.pyc -print0|xargs -0 rm -f

# Default fava port number
EXPOSE 5000

# Start the Fava webserver and await HTTP requests
# TODO: add some echo information that might be useful to the user
CMD fava $FAVA_OPTIONS $BEANCOUNT_INPUT_FILE
