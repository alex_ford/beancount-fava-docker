#!/usr/env python3

# Cross-platform Python script for starting arf/beancount-fava-docker container!

import logging
logging.basicConfig(level=logging.INFO)
_log = logging.getLogger(__name__)

import docker

# TODO: will need to figure out how to handle the path nuances between platforms
dockerClient = docker.from_env()
_log.info("Building Docker image: 'arf/beancount-fava-docker'")

cli = docker.APIClient(base_url="unix://var/run/docker.sock")
for line in cli.build(path = ".",
                      rm = True,
                      encoding = "utf-8",
                      tag = "arf/beancount-fava-docker"):
    _log.info(line.decode())

_log.info("Finished building the image.")
_log.info("Starting a container from the image...")

response = dockerClient.containers.run("arf/beancount-fava-docker",
    name = "beancount-fava-container",
    remove = True,
    volumes = {
        "/home/alex/Workspaces-Personal/beancount/beancount-data/": {
            "bind": "/data",
            "mode": "rw"
        }
    },
    working_dir = "/data",
    environment = {
        "BEANCOUNT_INPUT_FILE": "/data/main.beancount"
    },
    ports = {
        "5000/tcp": "5000"
    }
)
_log.info("Beancount Fava Docker container has started!")
_log.info("Try accessing it from your favorite browser at: http://localhost:5000")
