#!/bin/bash

docker run --rm -p 5000:5000 \
    -v `pwd`:/data \
    -e BEANCOUNT_INPUT_FILE=/data/ledger.beancount \
    --name fava \
    yegle/fava
