# Stop script for Beancount Fava Docker
#
# Attempts to stop everything, including the Docker container, the Docker 
# Machine, and then perform a sync of the beancount-data files.
#
# @author Alex Richard Ford (arf4188@gmail.com)

Write-Output "Stopping Beancount/Fava Docker container..."
docker stop fava

# TODO add this back in as an argument (e.g. -a,--all)
#docker-machine stop default

Write-Output "Pushing Beancount data file to Git repository..."
Push-Location -Path ..\beancount-data
.\git-sync.ps1
Pop-Location

Write-Output "Finished."