# Beancount Fava Quickapp

This is largely based around Fava, and following the install/setup/usage guidelines therein.

https://beancount.github.io/fava/
https://github.com/beancount/fava

TODO: consider breaking this into at least 2 containers:
1. Beancount/Fava
2. Google Chrome, appified and pointed at Fava

Then Docker Compose can tie it altogether very simply: `docker-compose up`
